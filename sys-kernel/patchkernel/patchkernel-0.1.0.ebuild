# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Utility for patching sys-kernel/gentoo-kernel"
HOMEPAGE="https://gitlab.com/pask1/patchkernel"
SRC_URI="https://gitlab.com/pask1/patchkernel/-/archive/v${PV}/patchkernel-v${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

S=${WORKDIR}/${PN}-v${PV}

src_install() {
	dosbin "${PN}"
	doman "${PN}.8"
}
